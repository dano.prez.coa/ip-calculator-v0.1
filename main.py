from re import S
from kivy.lang.builder import Builder
from kivymd.app import MDApp
from kivymd.uix.screen import MDScreen
from kivy.uix.screenmanager import ScreenManager
from kivy.properties import StringProperty
from kivymd.uix.dialog import MDDialog
from serviciosIP import serviciosIP

from kivy.core.window import Window
Window.keyboard_anim_args = {'d': .2, 't': 'in_out_expo'}
Window.softinput_mode = "below_target"

global subnet
global ipCompleta

class MainMenu (MDScreen):
    textSubnet = StringProperty()
    ip1 = StringProperty()
    ip2 = StringProperty()
    ip3 = StringProperty()
    ip4 = StringProperty()
    textSubnet = "32"
    ip1 = "192"
    ip2 = "168"
    ip3 = "0"
    ip4 = "1"

    def error1(self):
        self.dialog = MDDialog(
            text="Please check the fields and provide valid information.",
            radius=[20, 7, 20, 7],
        )
        self.dialog.open()

    def error2(self):
        self.dialog = MDDialog(
            text="Please provide a valid IP address.",
            radius=[20, 7, 20, 7],
        )
        self.dialog.open()

    def error3(self):
        self.dialog = MDDialog(
            text="Please provide a valid Prefix length.",
            radius=[20, 7, 20, 7],
        )
        self.dialog.open()

    def validateTransition(self):
        #Primero validamos que los campos no esten vacios o se haya ingresado un valor no númerico
        if(self.ids.txt_ip1.text=='' or self.ids.txt_ip2.text=='' or self.ids.txt_ip3.text=='' or self.ids.txt_ip4.text=='' or
            self.ids.subnet.text=='' or not(self.ids.txt_ip1.text.isnumeric()) or not(self.ids.txt_ip2.text.isnumeric()) or not(self.ids.txt_ip3.text.isnumeric())
            or not(self.ids.txt_ip4.text.isnumeric()) or not(self.ids.subnet.text.isnumeric())):
            MainMenu.error1(self)
            return False
        else:
            #Ahora comprobamos que esten en los ragos establecidos en las IPs 
            if((int(self.ids.txt_ip1.text) >255 or int(self.ids.txt_ip2.text) >255 or int(self.ids.txt_ip3.text) >255 or int(self.ids.txt_ip4.text) >255 or
               int(self.ids.txt_ip1.text) <0 or int(self.ids.txt_ip2.text) <0 or int(self.ids.txt_ip3.text) <0 or int(self.ids.txt_ip4.text) <0)):
                MainMenu.error2(self)
                return False
            else:
                #Finalmente comprobamos que el Prefix Lenght sea valido
                if (int(self.ids.subnet.text)>32 or int(self.ids.subnet.text)<=0):
                    MainMenu.error3(self)
                    return False
                else:
                    MainMenu.calculate(self)
                    return True

    def calculate(self):
        global ipCompleta
        global subnet
        subnet = self.ids.subnet.text
        ip1 = self.ids.txt_ip1.text
        ip2 = self.ids.txt_ip2.text
        ip3 = self.ids.txt_ip3.text
        ip4 = self.ids.txt_ip4.text
        ipCompleta = ip1 + "." + ip2 + "." + ip3 + "." + ip4

class Ip (MDScreen):
    ipTitulo1 = StringProperty()
    number = StringProperty()
    binaryIP = StringProperty()
    hexaIP = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        ipList = servicesIP.calculateIP(ipCompleta)
        self.ids.number.text = ipList[0]
        self.ids.binaryIP.text = ipList[1]
        self.ids.hexaIP.text = ipList[2]

class Broadcast (MDScreen):
    ipTitulo1 = StringProperty()
    numberBroadcast = StringProperty()
    binaryBroadCast = StringProperty()
    hexaBroadcast = StringProperty()
    def start(self):
        subnetResult=int(subnet)
        servicesIP = serviciosIP()
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        Bc = servicesIP.calculateBroadcast(ipCompleta, subnetResult)
        self.ids.numberBroadcast.text = Bc[0]
        self.ids.binaryBroadCast.text = Bc[1]
        self.ids.hexaBroadcast.text = Bc[2]

class Netmask (MDScreen):
    ipTitulo1 = StringProperty()
    numberNet = StringProperty()
    binaryNet = StringProperty()
    hexaNet = StringProperty()
    def start(self):
        subnetResult=int(subnet)
        servicesIP = serviciosIP()
        self.ids.ipTitulo1.text ="CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        Netmsk = servicesIP.calculateNetmask(subnetResult)
        self.ids.numberNet.text = Netmsk[0]
        self.ids.binaryNet.text = Netmsk[1]
        self.ids.hexaNet.text = Netmsk[2]

class MinHost (MDScreen):
    ipTitulo1 = StringProperty()
    numberMinH = StringProperty()
    binaryMinH  = StringProperty()
    hexaMinH  = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        subnetResult=int(subnet)
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        MinH = servicesIP.calcualteMinH(ipCompleta, subnetResult)
        self.ids.numberMinH.text = MinH[0]
        self.ids.binaryMinH.text = MinH[1]
        self.ids.hexaMinH.text = MinH[2]

class Wildcard (MDScreen):
    ipTitulo1 = StringProperty()
    numberWild = StringProperty()
    binaryWild = StringProperty()
    hexaWild = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        subnetResult=int(subnet)
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        WildC = servicesIP.calculateWildcard(ipCompleta, subnetResult)
        self.ids.numberWild.text = WildC[0]
        self.ids.binaryWild.text = WildC[1]
        self.ids.hexaWild.text = WildC[2]

class MaxHost (MDScreen):
    ipTitulo1 = StringProperty()
    numberMaxH = StringProperty()
    binaryMaxH = StringProperty()
    hexaMaxH = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        subnetResult=int(subnet)
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        MaxH = servicesIP.calcualteMaxH(ipCompleta, subnetResult)
        self.ids.numberMaxH.text = MaxH[0]
        self.ids.binaryMaxH.text = MaxH[1]
        self.ids.hexaMaxH.text = MaxH[2]

class Network (MDScreen):
    ipTitulo1 = StringProperty()
    numberNetwork = StringProperty()
    binaryNetwork = StringProperty()
    hexaNetwork = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        subnetResult=int(subnet)
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        NtWrk = servicesIP.calculateNetwork(ipCompleta, subnetResult)
        self.ids.numberNetwork.text = NtWrk[0]
        self.ids.binaryNetwork.text = NtWrk[1]
        self.ids.hexaNetwork.text = NtWrk[2]

class Details (MDScreen):
    ipTitulo1 = StringProperty()
    maxHosts = StringProperty()
    Class = StringProperty()
    def start(self):
        servicesIP = serviciosIP()
        subnetResult=int(subnet)
        self.ids.ipTitulo1.text = "CIDR IP Address Calculator \n"+ "IP: "+ipCompleta+"\n With prefix length: "+ subnet
        Dtl = servicesIP.calculateDetails(ipCompleta, subnetResult)
        self.ids.maxHosts.text = str(Dtl[0])
        self.ids.Class.text = Dtl[1]

class ChooseOptions (MDScreen):
        pass 

class WindowManager (ScreenManager):
    pass

class GuiCalc(MDApp):
    def build(self):
        kv=Builder.load_file('gui.kv')
        return kv

GuiCalc().run()