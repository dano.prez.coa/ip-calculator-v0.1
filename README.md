@Proy. de investigación UAM I 2022
Aplicación "IP Calculator".
####Profesor: Miguel Ángel Ruiz Sánchez
#### Daniel David Pérez Coapango
#### Joel Benjamín Pérez Giroud Guevara

El proyecto está dividido principalmente el 3 archivos.
El main: Que se encarga de gestionar las ventanas que se muestran en la aplicación y ejecutar la misma.
Servicios: Que se encarga de todas las operaciones aritmeticas que la calculadora realiza.
gui.kv: Que se encarga de toda la interfaz gráfica escrito en lenguaje de kivy. 
####
## Kivy
### Instalar en consola bash:
pip install kivy