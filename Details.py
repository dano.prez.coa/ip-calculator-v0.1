class Details:
    # Constructor
    def __init__(self, maxHost, Class):
        self.__maxHost = maxHost
        self.__Class = Class

    # Definimos Getter para maxHost
    def getMaxHost(self):
        return self.__maxHost

    # Definimos Setter para maxHost
    def setMaxHost(self, maxHost):
        self.__maxHost = maxHost

    # Definimos Getter para Class
    def getClass(self):
        return self.__Class

    # Definimos Setter para Class
    def setClass(self, Class):
        self.__Class = Class