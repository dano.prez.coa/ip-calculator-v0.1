class Response:
    #Constructor
    def __init__(self, num, binary, hexa):
        self.__num = num
        self.__binary = binary
        self.__hexa = hexa
        
    #Definimos Getter para num
    def getNum(self):
        return self.__num
    
    #Definimos Setter para num
    def setNum(self, num):
        self.__num = num

    # Definimos Getter para binary
    def getBinary(self):
        return self.__binary

    # Definimos Setter para binary
    def setBinary(self, num):
        self.__num = num

    # Definimos Getter para hexa
    def getHexa(self):
        return self.__hexa

    # Definimos Setter para hexa
    def setHexa(self, num):
        self.__num = num