import math
from Response import Response
from Details import Details

class serviciosIP:
    def __init__(self) -> None:
        pass

    # Metodos principales
    # Funcionalidad 1: Calcuate IP
    def calculateIP(self, ip):
        binario = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
            hexa.append(serviciosIP.decToHexa(i))
        for i in range(4):
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseIP = Response(ip, binarioString, hexaString)
        # Regresamos la respuesta
        return responseIP.getNum(), responseIP.getBinary(), responseIP.getHexa()

    # Funcionalidad 2: Calculate Netmask
    def calculateNetmask(self, subnet):
        binario = []
        decimal = []
        binarioCompleto = []
        hexa = []
        while subnet > 0:
            list = [0, 0, 0, 0, 0, 0, 0, 0]
            if subnet > 8:
                for i in range(8):
                    list[i] = 1
                binario.append(list)
            else:
                for i in range(subnet):
                    list[i] = 1
                binario.append(list)
                break
            subnet = subnet - 8
        serviciosIP.completeNetmask(binario)
        for i in range(4):
            decimal.append(serviciosIP.binaryToDec(binario[i]))
            binario[i].reverse()
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseNetmask = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseNetmask.getNum(), responseNetmask.getBinary(), responseNetmask.getHexa()

    # Funcionalidad 3: Wildcard
    def calculateWildcard(self, ip, subnet):
        binario = []
        decimal = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
        for i in range(4):
            subnet = serviciosIP.createWildcard(binario[i], subnet)
            decimal.append(serviciosIP.binaryToDec(binario[i]))
            binario[i].reverse()
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseWildcard = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseWildcard.getNum(), responseWildcard.getBinary(), responseWildcard.getHexa()

    # Funcionalidad 4: Network
    def calculateNetwork(self, ip, subnet):
        binario = []
        decimal = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
        for i in range(4):
            subnet = serviciosIP.createNetwork(binario[i], subnet)
            decimal.append(serviciosIP.binaryToDec(binario[i]))
            binario[i].reverse()
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseNetwork = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseNetwork.getNum(), responseNetwork.getBinary(), responseNetwork.getHexa()

    # Funcionalidad 5: Broadcast
    def calculateBroadcast(self, ip, subnet):
        binario = []
        decimal = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
        if subnet == 32:
            for i in range(4):
                decimal.append(serviciosIP.binaryToDec(binario[i]))
                binario[i].reverse()
                binarioCompleto.append(serviciosIP.listToString(binario[i]))
        else:
            for i in range(4):
                subnet = serviciosIP.createBroadcast(binario[i], subnet)
                decimal.append(serviciosIP.binaryToDec(binario[i]))
                binario[i].reverse()
                binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseBroadcast = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseBroadcast.getNum(), responseBroadcast.getBinary(), responseBroadcast.getHexa()

    # Funcionalidad 6: Minimum Host
    def calcualteMinH(self, ip, subnet):
        binario = []
        decimal = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
        for i in range(4):
            subnet = serviciosIP.createMinHost(binario[i], subnet, i)
            decimal.append(serviciosIP.binaryToDec(binario[i]))
            binario[i].reverse()
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseMinHost = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseMinHost.getNum(), responseMinHost.getBinary(), responseMinHost.getHexa()

    # Funcionalidad 7: Maximum Host
    def calcualteMaxH(self, ip, subnet):
        binario = []
        decimal = []
        hexa = []
        binarioCompleto = []
        ip2 = ip.split(sep='.')
        for i in ip2:
            binario.append(serviciosIP.decToBinary(i))
        for i in range(4):
            subnet = serviciosIP.createMaxHost(binario[i], subnet, i)
            decimal.append(serviciosIP.binaryToDec(binario[i]))
            binario[i].reverse()
            binarioCompleto.append(serviciosIP.listToString(binario[i]))
        decimalString = serviciosIP.listImpToString(decimal)
        ip2 = decimalString.split(sep='.')
        for i in ip2:
            hexa.append(serviciosIP.decToHexa(i))
        binarioString = serviciosIP.listImpToString(binarioCompleto)
        hexaString = serviciosIP.listImpToString(hexa)
        # Le pasamos los valores que deben tener
        responseMaxHost = Response(decimalString, binarioString, hexaString)
        # Regresamos la respuesta
        return responseMaxHost.getNum(), responseMaxHost.getBinary(), responseMaxHost.getHexa()

    # Funcionalidad 8: Details
    def calculateDetails(self, ip, subnet):
        clase = serviciosIP.classDirIp(ip)
        valMaxHost = serviciosIP.valMaxHost(subnet)
        # Le pasamos los valores que deben tener
        responseDetails = Details(valMaxHost, clase)
        # Regresamos la respuesta
        return responseDetails.getMaxHost(), responseDetails.getClass()

    # Transforma de decimal a binario
    def decToBinary(ip):
        list = [0, 0, 0, 0, 0, 0, 0, 0]
        x = int(ip)
        if x == 0:
            return list
        else:
            return serviciosIP.compareDec(list, x)

    # Transforma de decimal a Hexadecimal
    def decToHexa(ip):
        list = []
        x = int(ip)
        serviciosIP.resHexa(list, x)
        list.reverse()
        return serviciosIP.listToStringHexa(list)

    # Ve el tipo de clase al que pertenece la direccion IP
    def classDirIp(ip):
        ip2 = ip.split(sep='.')
        x = int(ip2[0])
        y = int(ip2[1])
        clase = serviciosIP.evalDirIP(x)
        clase = clase + serviciosIP.evalClase(x, y)
        # Imprime la clase a la que pertenece temporalmente
        return clase

    # Compara los numeros decimales recorriendo potencias de 2
    def compareDec(list, num):
        y = 7
        x = 2 ** y
        while num < x:
            y = y - 1
            x = 2 ** y
        resta = num - x
        serviciosIP.saveBinary(list, x)
        if resta != 0:
            serviciosIP.compareDec(list, resta)
        return list

    # Guarda los 1 necesarios para formar al binario
    def saveBinary(list, num):
        if num == 128:
            list[0] = 1
        if num == 64:
            list[1] = 1
        if num == 32:
            list[2] = 1
        if num == 16:
            list[3] = 1
        if num == 8:
            list[4] = 1
        if num == 4:
            list[5] = 1
        if num == 2:
            list[6] = 1
        if num == 1:
            list[7] = 1
        return list

    # Convierte la lista en un String para binario
    def listToString(list):
        value = ""
        for i in list:
            value = value + str(i)
        return value

    # Convierte la lista en String para hexadecimal
    def listToStringHexa(list):
        value = ""
        x = len(list)
        if x > 1:
            for i in list:
                value = value + str(i)
        else:
            for i in list:
                value = "0"
                value = value + str(i)
        return value

    # Convertimos la lista para imprimir en pantalla
    def listImpToString(list):
        value = ""
        for i in range(4):
            if i < 3:
                value += str(list[i]) + "."
            else:
                value += str(list[i])
        return value

    # Evaluamos los residuos dividiendo entre 16 y regresamos una lista
    def resHexa(list, num):
        div = num / 16
        pDecimal, pEntera = math.modf(div)
        if (pEntera) != 0:
            res = num % 16
            if res >= 10:
                serviciosIP.saveHexa(list, res)
            else:
                redondeo = int(res)
                list.append(redondeo)
            serviciosIP.resHexa(list, pEntera)
        else:
            res = num % 16
            if res >= 10:
                serviciosIP.saveHexa(list, res)
            else:
                redondeo = int(res)
                list.append(redondeo)
        return list

    # Guarda los valores necesarios para un numero hexadecimal
    def saveHexa(list, num):
        if num == 10:
            list.append("A")
        if num == 11:
            list.append("B")
        if num == 12:
            list.append("C")
        if num == 13:
            list.append("D")
        if num == 14:
            list.append("E")
        if num == 15:
            list.append("F")
        return list

    # Evaluamos a que clase corresponde la direccion IP
    def evalDirIP(num):
        if 0 <= num <= 126:
            return "A - "
        if 127 <= num <= 191:
            return "B - "
        if 192 <= num <= 223:
            return "C - "
        if 224 <= num <= 239:
            return "D - "
        if 240 <= num <= 255:
            return "E - "

    # Evaluamos si la dirección es pública o privada
    def evalClase(num1, num2):
        if (num1 == 10) or (num1 == 172 and (16 <= num2 <= 31)) or (num1 == 192 and num2 == 168):
            return "Private Network"
        else:
            return "Public Network"

    # Completamos la netmask
    def completeNetmask(binario):
        list = [0, 0, 0, 0, 0, 0, 0, 0]
        x = 4 - len(binario)
        for i in range(x):
            binario.append(list)
        return binario

    # Binario to decimal
    def binaryToDec(list):
        suma = 0
        list.reverse()
        for i in range(8):
            x = 2 ** i
            if list[i] == 1:
                suma = suma + x
        return suma

    # Calculamos el valor máximo de los hosts
    def valMaxHost(num):
        x = 32 - num
        maxHosts = 2 ** x
        if num < 31:
            maxHosts = maxHosts - 2
        return maxHosts

    # Creamos el broadcast poniendo en 1 todos los bits del host
    def createBroadcast(binarioList, num):
        num = num - 8
        if num <= 0:
            num = 8 + num
            if num != 8:
                for i in range(8):
                    if i >= num:
                        binarioList[i] = 1
                return 8
            else:
                for i in range(8):
                    binarioList[i] = 1
                return 8
        else:
            return num

    # Creamos el wildcard poniendo en 0 los bits de la red y en 1 los del hosts
    def createWildcard(binarioList, num):
        num = num - 8
        if num <= 0:
            num = 8 + num
            if num != 8:
                for i in range(8):
                    if i >= num:
                        binarioList[i] = 1
                    else:
                        binarioList[i] = 0
                return 8
            else:
                for i in range(8):
                    binarioList[i] = 1
                return 8
        else:
            for i in range(8):
                binarioList[i] = 0
            return num

    # Creamos la network poniendo en 0 todos los bits del host
    def createNetwork(binarioList, num):
        num = num - 8
        if num <= 0:
            num = 8 + num
            if num != 8:
                for i in range(8):
                    if i >= num:
                        binarioList[i] = 0
                return 8
            else:
                for i in range(8):
                    binarioList[i] = 0
                return 8
        else:
            return num

    # Ponemos toda la parte del hosts en 0 y en caso de estar en el ultimo
    # octeto de bits del hosts lo marcamos en 1
    def createMinHost(binarioList, num, valfinal):
        num = num - 8
        if num <= 0:
            num = 8 + num
            if num != 8:
                for i in range(8):
                    if i >= num:
                        binarioList[i] = 0
                if valfinal == 3:
                    binarioList[7] = 1
                return 8
            else:
                for i in range(8):
                    binarioList[i] = 0
                if valfinal == 3:
                    binarioList[7] = 1
                return 8
        else:
            return num

    # Ponemos toda la parte del hosts en 1 y en caso de estar en el ultimo
    # octeto de bits del hosts lo marcamos en 0
    def createMaxHost(binarioList, num, valfinal):
        num = num - 8
        if num <= 0:
            num = 8 + num
            if num != 8:
                for i in range(8):
                    if i >= num:
                        binarioList[i] = 1
                if valfinal == 3:
                    binarioList[7] = 0
                return 8
            else:
                for i in range(8):
                    binarioList[i] = 1
                if valfinal == 3:
                    binarioList[7] = 0
                return 8
        else:
            return num
